#include "mastertest.h"

#include <iostream>

// INCLUDE NEW TEST SUITES HERE - DO NOT REMOVE THIS LINE

#include "testsuites/example_test.cpp"


MasterTest::MasterTest() {

    //ADD NEW TEST SUITES HERE - DO NOT REMOVE THIS LINE
    testSuites.push_back(Example_Test());

}

std::vector<TestResult> MasterTest::runAllTests() {

    std::vector<TestResult> failedTests;

    for( unsigned int i = 0; i < testSuites.size(); i++ ) {
        std::vector<TestResult> testSuiteFailedTests = testSuites[i].runAllTests();
        failedTests.insert(failedTests.end(), testSuiteFailedTests.begin(), testSuiteFailedTests.end()) ;
    }

    return failedTests;

}

std::vector<TestResult> MasterTest::runTests(std::vector<int> indices) {
    std::vector<TestResult> failedTests;

    for( unsigned int i = 0; i < indices.size(); i++ ) {
        std::vector<TestResult> testSuiteFailedTests = testSuites[indices[i]].runAllTests();
        failedTests.insert(failedTests.end(), testSuiteFailedTests.begin(), testSuiteFailedTests.end()) ;
    }

    return failedTests;
}

void MasterTest::printTestSuites() {
    std::cout << "To run all tests, run without arguments or with '-a'" << std::endl;
    std::cout << "To run subset of test suites, list suite indexes as \n    individual arguments or grouped (eg. '0-3')" << std::endl;
    std::cout << "Available Test Suites: " << std::endl;

    for(unsigned int i=0; i<testSuites.size(); i++) {
        std::cout << i << ": " << testSuites[i].testSuiteName << std::endl;
    }
}

int MasterTest::numTests() {
    int total = 0;
    for( unsigned int i = 0; i < testSuites.size(); i++ ) {
        total += testSuites[i].numTests();
    }

    return total;
}

int MasterTest::numTests(std::vector<int> indices) {
    int total = 0;
    for( unsigned int i = 0; i < indices.size(); i++ ) {
        total += testSuites[indices[i]].numTests();
    }

    return total;
}

bool MasterTest::validSubTests(std::vector<int> indices) {
    for(unsigned int i=0; i<indices.size(); i++) {
        if(indices[i]<0||indices[i]>=(int)testSuites.size()) {
            return false;
        }
    }

    return true;
}
